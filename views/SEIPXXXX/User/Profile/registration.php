<?php
include_once('../../../../vendor/autoload.php');
use App\BITM\SEIPXXXX\User\User;
use App\BITM\SEIPXXXX\User\Auth;
use App\BITM\SEIPXXXX\Message\Message;
use App\BITM\SEIPXXXX\Utility\Utility;
$auth= new Auth();
$status= $auth->setData($_POST)->is_exist();
if($status){
    Message::setMessage("<div class='alert alert-danger'>
    <strong>Taken!</strong> Email has already been taken. </div>");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}else {
    $_POST['email_token'] = md5(uniqid(rand()));
    $obj = new User();
    $obj->setData($_POST)->store();
    require '../../../../vendor/phpmailer/phpmailer/PHPMailerAutoload.php';
    $yourGmailAddress = 'bspipublished123@gmail.com';
    $yourGmailPassword = 'bspi1234';
    $mail = new PHPMailer;

    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );
    //Tell PHPMailer to use SMTP
    $mail->isSMTP();
    //Enable SMTP debugging
    // 0 = off (for production use)
    // 1 = client messages
    // 2 = client and server messages
    $mail->SMTPDebug = 0;
    //Ask for HTML-friendly debug output
    $mail->Debugoutput = 'html';
    //Set the hostname of the mail server
    $mail->Host = 'smtp.gmail.com';
    // use
    // $mail->Host = gethostbyname('smtp.gmail.com');
    // if your network does not support SMTP over IPv6
    //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
    $mail->Port = 587; //587
    //Set the encryption system to use - ssl (deprecated) or tls
    $mail->SMTPSecure = 'tls'; //tls
    //Whether to use SMTP authentication
    $mail->SMTPAuth = true;
    //Username to use for SMTP authentication - use full email address for gmail
    $mail->Username = $yourGmailAddress;
    //Password to use for SMTP authentication
    $mail->Password = $yourGmailPassword;
    //Set who the message is to be sent from
    $mail->setFrom($yourGmailAddress, 'BITM PHP');
    //Set an alternative reply-to address
    $mail->addReplyTo($yourGmailAddress, 'BITM PHP');

    $mail->addAddress($_POST['email']);
    $mail->AltBody = 'This is a plain-text message body';

    $mail->Body = "this is the body";

    $mail->Subject = "Your Account Activation Link";
    $message = "
       Please click this link to verify your account:
       http://localhost/UserManagement/views/SEIPXXXX/User/Profile/emailverification.php?email=" . $_POST['email'] . "&email_token=" . $_POST['email_token'];


    $mail->MsgHTML($message);
    $mail->Send();
    if (!$mail->send()) {
        echo "<span style='color: green'>Mailer Error: </span>" . $mail->ErrorInfo;
    } else {
        Message::message("<span style='color: green;'><strong>Success!</strong> Email has been sent successfully.</span>");


    }
}